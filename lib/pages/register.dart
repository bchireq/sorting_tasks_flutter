
import 'package:authentication_repository/authentication_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {

  @override
  State<RegisterPage> createState() => _RegisterPage();
}

class _RegisterPage extends State<RegisterPage> {
  late Future<User> _futureUser;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final AuthenticationRepository _authRepository = AuthenticationRepository();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height,
            maxWidth: MediaQuery.of(context).size.width,
          ),
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/img/fondo.png'),
                fit: BoxFit.cover),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 36.0, horizontal: 24.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children:  [
                      Container(
                        height: 120,
                        width: double.infinity,
                        alignment: Alignment.center,
                        child:
                        Container(
                          width: 140,
                          height: 140,
                          child: Image.asset(
                            "assets/img/note.png",
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 5.0,
                      ),
                      const Center(
                        child: Text("Sorting Tasks",
                          style: TextStyle(
                              color: Color(0xFFFFD845),
                              fontSize: 28.0,
                              fontFamily: 'Roboto'
                          ),
                        ),
                      )
                    ],
                  )
                )
              ),
              Expanded(
                flex: 7,
                child: Container(
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                    )
                  ),
                  child: Padding (
                    padding: const EdgeInsets.all(24.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text("Registrarse",
                          style: TextStyle(
                              color: Color(0xFF0B1C48),
                              fontSize: 28.0,
                              fontFamily: 'Roboto'
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        TextField(
                          controller: _nameController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                borderSide: BorderSide.none,
                              ),
                              filled: true,
                              fillColor: Color(0xFFe7edeb),
                              hintText: "Usuario",
                              prefixIcon: const Icon(
                                Icons.email,
                                color: Color(0xFF0B1C48),
                              )
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        TextField(
                          controller: _emailController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide.none,
                            ),
                            filled: true,
                            fillColor: Color(0xFFe7edeb),
                            hintText: "Correo Electrónico",
                            prefixIcon: const Icon(
                              Icons.email,
                              color: Color(0xFF0B1C48),
                            )
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        TextField(
                          controller: _passwordController,
                          keyboardType: TextInputType.emailAddress,
                          obscureText: true,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                borderSide: BorderSide.none,
                              ),
                              filled: true,
                              fillColor: Color(0xFFe7edeb),
                              hintText: "Contraseña",
                              prefixIcon: const Icon(
                                Icons.lock,
                                color: Color(0xFF0B1C48),
                              )
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        TextField(
                          keyboardType: TextInputType.emailAddress,
                          obscureText: true,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                borderSide: BorderSide.none,
                              ),
                              filled: true,
                              fillColor: Color(0xFFe7edeb),
                              hintText: "Repetir Contraseña",
                              prefixIcon: const Icon(
                                Icons.lock,
                                color: Color(0xFF0B1C48),
                              )
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          height: 45.0,
                          width: double.infinity,
                          child: Material(
                            borderRadius: BorderRadius.circular(10.0),
                            shadowColor: const Color(0xFF0C2965),
                            color: const Color(0xFF0B1C48),
                            elevation: 7.0,
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  _authRepository
                                      .signUp(
                                    email: _emailController.text,
                                    password: _passwordController.text,
                                  )
                                      .then((_) => {
                                    Navigator.of(context)
                                        .pushNamed('/user')
                                  });
                                });
                              },
                              child: const Center(
                                child: Text(
                                  'Registrate',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontFamily: 'Roboto'
                                  ),
                                ),
                              )
                            )
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              )
            ],
          ),
        ),
      )
    );
  }
}
