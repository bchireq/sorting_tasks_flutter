
import 'package:authentication_repository/authentication_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sorting_tasks/widgets/sidebar.dart';

class ProfilePage extends StatefulWidget {

  @override
  State<ProfilePage> createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage> {

  late Future<User> _futureUser;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final AuthenticationRepository _authRepository = AuthenticationRepository();

  @override
  void initState() {
    super.initState();
    _futureUser = _authRepository.user.first;
    _futureUser.then((value) {
      _emailController.text = value.email!;
      if(value.name != null){
        _usernameController.text = value.name!;
      }else{
        _usernameController.text = "user";
      }
      _passwordController.text = "goodbye_everybody";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF4A71C2),
      ),
      drawer: const SideBar(),
      body: SingleChildScrollView(
        child: FutureBuilder<User>(
          future: _futureUser,
          builder: (context, snapshot){
            if (snapshot.hasData) {
              return Container(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height,
                  maxWidth: MediaQuery.of(context).size.width,
                ),
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/img/fondo.png'),
                      fit: BoxFit.cover),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        flex: 4,
                        child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 36.0, horizontal: 24.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children:  [
                                Container(
                                  height: 190,
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                  child:
                                  Container(
                                    width: 190,
                                    height: 190,
                                    child: Image.asset(
                                      "assets/img/profile.png",
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                              ],
                            )
                        )
                    ),
                    Expanded(
                        flex: 7,
                        child: Container(
                          width: double.infinity,
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40),
                                topRight: Radius.circular(40),
                              )
                          ),
                          child: Padding (
                            padding: const EdgeInsets.all(24.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Text("Mi información",
                                  style: TextStyle(
                                      color: Color(0xFF0B1C48),
                                      fontSize: 28.0,
                                      fontFamily: 'Roboto'
                                  ),
                                ),
                                const SizedBox(
                                  height: 20.0,
                                ),
                                TextField(
                                  enabled: false,
                                  controller: _usernameController,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8.0),
                                        borderSide: BorderSide.none,
                                      ),
                                      filled: true,
                                      fillColor: Color(0xFFe7edeb),
                                      hintText: "Usuario",
                                      prefixIcon: const Icon(
                                        Icons.person,
                                        color: Color(0xFF0B1C48),
                                      )
                                  ),
                                ),
                                const SizedBox(
                                  height: 20.0,
                                ),
                                TextField(
                                  enabled: false,
                                  controller: _emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8.0),
                                        borderSide: BorderSide.none,
                                      ),
                                      filled: true,
                                      fillColor: Color(0xFFe7edeb),
                                      hintText: "Correo Electrónico",
                                      prefixIcon: const Icon(
                                        Icons.email,
                                        color: Color(0xFF0B1C48),
                                      )
                                  ),
                                ),
                                const SizedBox(
                                  height: 20.0,
                                ),
                                TextField(
                                  enabled: false,
                                  controller: _passwordController,
                                  keyboardType: TextInputType.emailAddress,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8.0),
                                        borderSide: BorderSide.none,
                                      ),
                                      filled: true,
                                      fillColor: Color(0xFFe7edeb),
                                      hintText: "Contraseña",
                                      prefixIcon: const Icon(
                                        Icons.lock,
                                        color: Color(0xFF0B1C48),
                                      )
                                  ),
                                ),
                                const SizedBox(
                                  height: 20.0,
                                ),
                                Container(
                                  height: 45.0,
                                  width: double.infinity,
                                  color: Colors.transparent,
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _authRepository
                                            .logOut()
                                            .then((_) => {
                                          Navigator.of(context)
                                              .pushNamed('/login')
                                        });
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: const Color(0xFF0B1C48),
                                              style: BorderStyle.solid,
                                              width: 1.0),
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.circular(10.0)),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: const <Widget>[
                                          Center(
                                            child: ImageIcon(
                                                AssetImage('assets/img/logout.png')),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Center(
                                              child: Text(
                                                'Cerrar Sesión',
                                                style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily: 'Roboto',
                                                  color: Color(0xFF0B1C48),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                    )
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        )
      )
    );
  }
}
