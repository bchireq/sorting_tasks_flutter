
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sorting_tasks/widgets/list_data.dart';
import 'package:sorting_tasks/widgets/searchbar.dart';
import 'package:sorting_tasks/widgets/sidebar.dart';

class NotePage extends StatefulWidget {

  @override
  State<NotePage> createState() => _NotePage();
}

class _NotePage extends State<NotePage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xFF4A71C2),
        onPressed: () {
          Navigator.of(context).pushNamed('/addnota');
        },
        child: const Icon(Icons.add),
      ),
      appBar: AppBar(
        backgroundColor: const Color(0xFF4A71C2),
      ),
      drawer: const SideBar(),
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height,
            maxWidth: MediaQuery.of(context).size.width,
          ),
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/img/fondo.png'),
                fit: BoxFit.cover),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SearchBar(),
              Listdata()
            ],
          ),
        ),
      )
    );
  }
}
