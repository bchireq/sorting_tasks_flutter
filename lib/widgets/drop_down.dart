import 'package:flutter/material.dart';
class DropDownWidget extends StatefulWidget {
  const DropDownWidget({Key? key}) : super(key: key);

  @override
  State<DropDownWidget> createState() => _DropDownWidgetState();
}

class _DropDownWidgetState extends State<DropDownWidget> {
  String dropdownValue = 'Tareas';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      alignment: Alignment.topLeft,
      isExpanded: true,
      value: dropdownValue,
      icon: const Icon(Icons.category),
      elevation: 30,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 2,
        color: Color(0xFF4A71C2),
      ),
      onChanged: (String? newValue) {
        setState(() {
          dropdownValue = newValue!;
        });
      },
      items: <String>['Tareas', 'Recordatorios', 'Eventos Importantes']
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
