import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SideBar extends StatelessWidget{
  const SideBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
                decoration: const BoxDecoration(
                  color: Color(0xFF0B1C48),
                ),
                child: Column(
                  children: [
                    Expanded(
                        child: Column(
                          children: [
                            Container(
                              height: 80,
                              width: double.infinity,
                              child: Container(
                                width: 100,
                                height: 100,
                                child: Image.asset(
                                  "assets/img/note.png",
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 5.0,
                            ),
                            const Text(
                              "Sorting Tasks",
                              style: TextStyle(
                                  color: Color(0xFFFFD845),
                                  fontSize: 20.0,
                                  fontFamily: 'Roboto'),
                            ),
                          ],
                        )
                    ),
                  ],
                )
            ),
            ListTile(
              leading: Icon(Icons.task),
              title: Text('Notas'),
                onTap: () {
                  Navigator.pushNamed(context, '/notas');
                }
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text('Mi perfil'),
              onTap: () {
                Navigator.pushNamed(context, '/user');
              }
            ),
          ],
        )
    );
  }
}