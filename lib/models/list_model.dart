import 'package:flutter/material.dart';

class Product {
  final String title, desc;
  final Color color;
  Product({
    required this.title,
    required this.desc,
    required this.color,
  });
}

List<Product> products = [
  Product(
    title: "Comprar Comida",
    color: Color(0xFF71b8ff),
    desc:
        "Para la cena navideña es necesario comprar los alimentos para el relleno del pavo",
  ),
  Product(
    title: "Reunión del Trabajo",
    color: Color(0xFFff6374),
    desc:
        "Hoy a las 4 de la tarde tengo reunión importante",
  ),
  Product(
    title: "Entrega trabajo final",
    color: Color(0xFFffaa5b),
    desc:
        "Coordinar con Erick la entrega del trabajo final",
  ),
  Product(
    title: "Regalos de navidad",
    color: Color(0xFF9ba0fc),
    desc:
        "Alistar los regalos para mis padres",
  ),
];
