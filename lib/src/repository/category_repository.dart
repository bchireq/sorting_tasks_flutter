import 'package:firebase_repository/entity_repository.dart';
import 'package:sorting_tasks/src/model/category.dart';

class CategoryRepository extends FirebaseRepository<Category> {
  Future<List<Category>> findByUserId(String userId) {
    return collection
        .where(
          'user_id',
          isEqualTo: userId,
        )
        .get()
        .then((querySnapshot) =>
            querySnapshot.docs.map((doc) => doc.data() as Category).toList());
  }

  @override
  Category toEntity(Map<String, dynamic> jsonEntity) {
    return Category(
      userId: jsonEntity['user_id'],
      name: jsonEntity['name'],
    );
  }

  @override
  Map<String, dynamic> toJsonMap(Category entity) {
    return <String, dynamic>{
      'user_id': entity.userId,
      'name': entity.name,
    };
  }
}
