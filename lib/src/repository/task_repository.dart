import 'package:firebase_repository/entity_repository.dart';
import 'package:sorting_tasks/src/model/task.dart';

class TaskRepository extends FirebaseRepository<Task> {
  Future<List<Task>> findByUserId(String userId) {
    return collection
        .where(
          'user_id',
          isEqualTo: userId,
        )
        .get()
        .then((querySnapshot) =>
            querySnapshot.docs.map((doc) => doc.data() as Task).toList());
  }

  @override
  Task toEntity(Map<String, dynamic> jsonEntity) {
    return Task(
      userId: jsonEntity['user_id'],
      title: jsonEntity['title'],
      description: jsonEntity['description'],
      startTime: DateTime.parse(jsonEntity['start_time']),
      endTime: DateTime.parse(jsonEntity['end_time']),
    );
  }

  @override
  Map<String, dynamic> toJsonMap(Task entity) {
    return <String, dynamic>{
      'user_id': entity.userId,
      'title': entity.title,
      'description': entity.description,
      'start_time': entity.startTime.toIso8601String(),
      'end_time': entity.endTime.toIso8601String(),
    };
  }
}
