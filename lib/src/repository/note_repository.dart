import 'package:firebase_repository/entity_repository.dart';
import 'package:sorting_tasks/src/model/note.dart';

class NoteRepository extends FirebaseRepository<Note> {
  Future<List<Note>> findByUserId(String userId) {
    return collection
        .where(
          'user_id',
          isEqualTo: userId,
        )
        .get()
        .then((querySnapshot) =>
            querySnapshot.docs.map((doc) => doc.data() as Note).toList());
  }

  @override
  Note toEntity(Map<String, dynamic> jsonEntity) {
    return Note(
      userId: jsonEntity['user_id'],
      categoryId: jsonEntity['category_id'],
      title: jsonEntity['title'],
      description: jsonEntity['description'],
      date: DateTime.parse(jsonEntity['date']),
      notificationDate: DateTime.parse(jsonEntity['notification_date']),
      notification: jsonEntity['notification'],
    );
  }

  @override
  Map<String, dynamic> toJsonMap(Note entity) {
    return <String, dynamic>{
      'user_id': entity.userId,
      'category_id': entity.categoryId,
      'title': entity.title,
      'description': entity.description,
      'date': entity.date.toIso8601String(),
      'notification_date': entity.notificationDate.toIso8601String(),
      'notification': entity.notification,
    };
  }
}
