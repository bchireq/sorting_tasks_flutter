import 'package:firebase_repository/entity_repository.dart';

class Task extends FirebaseEntity {
  String userId;
  String title;
  String? description;
  DateTime startTime;
  DateTime endTime;

  Task({
    String? documentId,
    required this.userId,
    required this.title,
    required this.description,
    required this.startTime,
    required this.endTime,
  }) : super(documentId: documentId);
}
