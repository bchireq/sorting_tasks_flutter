import 'package:firebase_repository/entity_repository.dart';

class Category extends FirebaseEntity {
  final String userId;
  String name;

  Category({
    String? documentId,
    required this.userId,
    required this.name,
  }) : super(documentId: documentId);
}
