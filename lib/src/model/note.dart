import 'package:firebase_repository/entity_repository.dart';

class Note extends FirebaseEntity {
  final String userId;
  String categoryId;
  String title;
  String description;
  DateTime date;
  DateTime notificationDate;
  bool notification;

  Note({
    required this.userId,
    required this.categoryId,
    required this.title,
    required this.description,
    required this.date,
    required this.notificationDate,
    required this.notification,
    String? documentId,
  }) : super(documentId: documentId);
}
