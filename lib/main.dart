import 'package:authentication_repository/authentication_repository.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sorting_tasks/pages/add_note.dart';
import 'package:sorting_tasks/pages/login.dart';
import 'package:sorting_tasks/pages/note.dart';
import 'package:sorting_tasks/pages/profile.dart';
import 'package:sorting_tasks/pages/register.dart';
import 'package:sorting_tasks/src/model/note.dart';
import 'package:sorting_tasks/src/repository/note_repository.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final authenticationRepository = AuthenticationRepository();
  final noteRepository = NoteRepository();
  noteRepository.create(Note(
    userId: "userId",
    categoryId: "categoryId",
    title: "title",
    description: "description",
    date: DateTime.now(),
    notificationDate: DateTime.now(),
    notification: true,
  ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        routes: <String, WidgetBuilder>{
          '/login': (BuildContext context) => LoginPage(),
          '/registrarse': (BuildContext context) => RegisterPage(),
          '/user': (BuildContext context) => ProfilePage(),
          '/notas': (BuildContext context) => NotePage(),
          '/addnota': (BuildContext context) => AddNote(),
        },
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: LoginPage());
  }
}
