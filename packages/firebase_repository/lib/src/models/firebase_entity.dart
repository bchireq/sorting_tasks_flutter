import 'package:equatable/equatable.dart';

abstract class FirebaseEntity extends Equatable {
  String? documentId;

  FirebaseEntity({this.documentId});

  List<Object> get props => [];
}
