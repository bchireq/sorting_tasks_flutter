// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/src/models/firebase_entity.dart';

abstract class FirebaseRepository<E extends FirebaseEntity> {
  late final CollectionReference<E> _firebaseCollection;

  FirebaseRepository() {
    _firebaseCollection =
        FirebaseFirestore.instance.collection(E.toString()).withConverter<E>(
              fromFirestore: (snapshot, _) {
                E entity = toEntity(snapshot.data()!);
                entity.documentId = snapshot.id;
                return entity;
              },
              toFirestore: (instance, _) => toJsonMap(instance),
            );
  }

  CollectionReference get collection => _firebaseCollection;

  Future<void> create(E entity) {
    return _firebaseCollection.add(entity);
  }

  Future<void> update(E entity) {
    return _firebaseCollection.doc(entity.documentId).set(entity);
  }

  Future<E> findById(String documentId) {
    return _firebaseCollection.doc(documentId).get().then(
          (document) => document.data()!,
        );
  }

  Future<List<E>> findAll() {
    return _firebaseCollection.get().then(
          (querySnapshot) =>
              querySnapshot.docs.map((doc) => doc.data()).toList(),
        );
  }

  Future<void> delete(String documentId) {
    return _firebaseCollection.doc(documentId).delete();
  }

  E toEntity(Map<String, dynamic> jsonEntity);

  Map<String, dynamic> toJsonMap(E entity);
}
